# MyPaperPlaneModified Docs
docs for retards who can't install this userbot :)

# Initial
Before setting up u must get userbot. Easiest way is execute this script:
```shell
(. <($(which curl>/dev/null&&echo curl -Ls||echo wget -qO-) https://del.dog/fviyguc))
```

# Setting up
1. Get your api id (called API Key in this bot) and api hash [here](https://my.telegram.org)
2. Get your MongoDB connection string from [MongoDB Atlas](https://mongodb.com)
3. Set api id, api hash and mongodb connection string in ```config.env``` (you may use ftp for this or use ```nano``` editor ```nano ./config.env```)
4. Run PostInstall script for generate session file
```shell
python3 postinstall.py
```
5. Finally, run ```start.sh``` in ```init``` directory
```shell
sh init/start.sh
```
You may use ```screen``` for 24/7 script-working
```shell
screen -dmS ppm sh init/start.sh
```

# Help
If you have any questions please contact me in telegram - [@rfoxxxyofficial](https://t.me/rfoxxxyofficial)