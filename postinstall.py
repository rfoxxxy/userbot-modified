# Copyright (C) 2019 rfoxxxy (@rfoxxxyofficial)
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#

import os

from dotenv import load_dotenv
from telethon import TelegramClient
import socks

load_dotenv("config.env")

API_KEY = os.environ.get("API_KEY", "")
API_HASH = os.environ.get("API_HASH", "")


while True:
	proxy = input('Do you want to use a proxy for authorization?(y/n) Note: you can configure userbot proxy in the config.env: ')
	answers = ['y', 'n', 'д', 'н', 'yes', 'no', 'да', 'нет']
	if proxy.lower() not in answers:
		print('Please answer with yes or no (y/n)')
	else:
		break

if proxy in ['y', 'д', 'yes', 'да']:
	useProxy = True
	while True:
		proxyurl = input('Please enter SOCKS5 Proxy URL and port (url:port): ')
		if not proxyurl:
			print('Enter proxy url and port')
		else:
			try:
				lmao = proxyurl.split(':')
				proxyURI = lmao[0]
				proxyPort = lmao[1]
			except:
				print('Please enter URL in the following format: URL:Port')
			finally:
				break
else:
	useProxy = False

while True:
	twoFac = input('Does your account have enabled 2FA?(y/n): ')
	answers = ['y', 'n', 'д', 'н', 'yes', 'no', 'да', 'нет']
	if twoFac.lower() not in answers:
		print('Please answer with yes or no (y/n)')
	else:
		break

if twoFac in ['y', 'д', 'yes', 'да']:
	use2FA = True
	while True:
		twoFApass = input('Please enter your 2FA password: ')
		if not twoFApass:
			print('Enter your 2FA password please')
		else:
			break
else:
	use2FA = False

if not useProxy:
	bot = TelegramClient('userbot', API_KEY, API_HASH)
else:
	bot = TelegramClient('userbot', API_KEY, API_HASH, proxy=(socks.SOCKS5, proxyURI, proxyPort))

if not use2FA:
	bot.start()
else:
	bot.start(password=str(twoFApass))